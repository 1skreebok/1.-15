# Задача 1.
# Реализовать класс, который наследуется от класса списка и переопределяет метод append.
# Вместо добавления элемента в список, требуется возводить в степень все элементы списка на переданное
# в append значение

class MyList(list):
    def append(self, value) -> None:
        for i in range(len(self)):
            self[i] **= value

a = MyList([1, 2, 3, 4])
print(a)
a.append(5)
print(*a, sep='\n')
